# modular content modeling demo

This sample code supports the article [Modular content modeling in the JAMstack framework with Forestry's Front Matter Templates and Hugo](Modular content modeling in the JAMstack framework with Forestry's Front Matter Templates and Hugo).


## Clone
`git clone --recurse-submodules https://gitlab.com/nicfits/modular-content-modeling-demo.git`

N.B.: you must use the `--recurse-submodules` flag when cloning!  The hugo theme slim is included as a submodule.

## Thanks

* https://github.com/zhe/hugo-theme-slim
* http://forestry.io
* https://gohugo.io
* DJ Walker for laying the groundwork in https://forestry.io/blog/3-tips-for-mastering-blocks/
